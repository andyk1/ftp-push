# FTP Push and SFTP Push

These scripts are based on a simple premise: Systems save files to the server
this runs on and create a blank trigger file when the files are ready. These
scripts see the trigger file, upload the files to a remote SFTP/FTP server and
moves them to a different folder.

## Example server layout

These examples are based on a server that has a folder at `/custfiles/FTP_PUSH`
shared (e.g. via Samba) and a folder `/ftppush` with these scripts and their
configuration files in.

There is also a directory called `/ftppush/BLANK` with no files in, this exists
as a safety net to prevent uploading the wrong files in cases where the
expected folder under `/custfiles/FTP_PUSH` has been removed.

## Scripts

Please read the example scripts carefully and make sure you understand them
before using them in a production environment, you **have** to make some
changes.

### sftp-push.sh

This is an example script for uploading to an SFTP server. Make a copy and
update the configuration variables as needed.

This script is set up for RSA or DSA certificate authentication, the secret key
of the RSA or DSA keypair needs to correspond to ssh file permission
requirements:

* Owned by the user running the script
* Permisison set to `-rw-------` (0600)

The script also requires that the public key of the remote server be added to
the file refernced by the `KNOWNHOSTS` variable before it is run via cron.
In most cases it is sufficient to uncomment lines 26 and 27 after setting the
configuration variables, run the script manually so you can respond to the sftp
prompt to add the host to the known hosts list, and then comment those lines
out again.

### ftp-push.sh

This is an example script for uploading to an FTP server. Make a copy and
update the configuration variables as needed.

This script requires an ncftp configuration file with the remote server name,
login ID and password in it. For security purposes it is a good idea to make
this file conform to the same file access permissions as ssh key files:

* Owned by the user running the script
* Permisison set to `-rw-------` (0600)
