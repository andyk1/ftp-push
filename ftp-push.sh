#!/bin/bash
# The FTP Push script is run via a cron schedule as needed for the specific set of files.
# Very often a once-per-day schedule is just fine.

# This is an example script, adjust the config as needed:

# A blank folder (this is used as a safety net)
BLANKDIR=/ftppush/BLANK
# The folder where the user dumps files to upload
INDIR=/custfiles/FTP_PUSH/username/new
# The folder uploaded files are moved to (make /dev/null to delete them)
OUTDIR=/custfiles/FTP_PUSH/username/done
# The trigger file the user creates when the files are ready
TRIGGER=/custfiles/FTP_PUSH/username/new/GO
# The ncftp config file to connect to the repote server see the eaxmple in username.cfg
FTPCONF=/ftppush/username.cfg
# Folder on the remote server to upload to
FTPDIR=/files/

# Some safety settiongs
umask 0000
export PATH=/usr/sbin:/usr/bin:/sbin:/bin

# Do we have anything to do?
if [ -f "${TRIGGER}" ] ; then
    # Remove the trigger file
    rm -f "${TRIGGER}"

    # Go to the empty safety net folder
    cd "${BLANKDIR}"

    # Now try to go to the input folder
    cd "${INDIR}"

    if ncftpput -f "${FTPCONF}" "${FTPDIR}" * ; then
        echo Upload OK, moving files to done
        mv * "${OUTDIR}"
    else
        echo Upload error, not moving files
    fi

fi
